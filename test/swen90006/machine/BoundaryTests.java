package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
	@Test
	public void test1() {
		/* mutation: regs[src1] +1 */
		List<String> list = new ArrayList<String>();
		list.add("MOV R0 3");
		list.add("MOV R1 1");
		list.add("DIV R2 R0 R1");
		list.add("RET R2");
		Machine m = new Machine();
		// the assertTrue method is used to check whether something holds.
		assertEquals(3, m.execute(list));
	}

	@Test(expected = InvalidInstructionException.class)
	public void test2() {
		List<String> list = new ArrayList<String>();
		list.add("MOV R33 33");
		list.add("RET R2");
		Machine m = new Machine();
		m.execute(list);
	}

	@Test(expected = InvalidInstructionException.class)
	public void test3() {
		List<String> list = new ArrayList<String>();
		list.add("MOV R0 1");
		list.add("JMP 1 1");
		list.add("RET R2");
		Machine m = new Machine();
		m.execute(list);
	}

	@Test
	public void test4() {
		/* mutation: regs[src1] +1 */
		List<String> list = new ArrayList<String>();
		list.add("RET R19");
		Machine m = new Machine();
		// the assertTrue method is used to check whether something holds.
		assertEquals(0, m.execute(list));
	}

	@Test
	public void test5() {
		/* mutation: regs[src1] +1 */
		List<String> list = new ArrayList<String>();
		list.add("LDR R0 R1 655");
		list.add("RET R0");
		Machine m = new Machine();
		// the assertTrue method is used to check whether something holds.
		assertEquals(0, m.execute(list));
	}

	@Test(expected = NoReturnValueException.class)
	public void test6() {
		/* mutation: regs[src1] +1 */
		List<String> list = new ArrayList<String>();
		list.add("");
		Machine m = new Machine();
		// the assertTrue method is used to check whether something holds.
		assertEquals(0, m.execute(list));
	}

	@Test
	public void test7() {
		/* mutation: regs[src1] +1 */
		List<String> list = new ArrayList<String>();
		list.add("MOV R3 10");
		list.add("MOV R2 1");
		list.add("MOV R1 0");
		list.add("MOV R0 100");
		list.add("SUB R4 R3 R1");
		list.add("JZ  R4 5");
		list.add("STR R0 0  R1");
		list.add("ADD R1 R1 R2");
		list.add("ADD R0 R0 R2");
		list.add("JMP -5");
		list.add("MOV R1 0");
		list.add("MOV R0 100");
		list.add("MOV R5 0");
		list.add("SUB R4 R3 R1");
		list.add("JZ  R4 6");
		list.add("LDR R4 R0 0");
		list.add("ADD R5 R5 R4");
		list.add("ADD R0 R0 R2");
		list.add("ADD R1 R1 R2");
		list.add("JMP -6");
		list.add("RET R5");
		list.add("");
		list.add("");
		Machine m = new Machine();
		// the assertTrue method is used to check whether something holds.
		assertEquals(45, m.execute(list));
	}

	@Test
	public void test8() {
		/* mutation: regs[src1] +1 */
		List<String> list = new ArrayList<String>();
		for (int i = 0; i <= 31; i++) {
			String command="MOV R"+i+" 5";
			list.add(command);
		}
		list.add("RET R0");
		Machine m = new Machine();
		// the assertTrue method is used to check whether something holds.
		assertEquals(5, m.execute(list));
	}
	
	@Test(expected = InvalidInstructionException.class)
	public void test9() {
		List<String> list = new ArrayList<String>();
		list.add("MOV R-1 1");
		Machine m = new Machine();
		// the assertTrue method is used to check whether something holds.
		m.execute(list);
	}
	
	@Test(expected = InvalidInstructionException.class)
	public void test10() {
		List<String> list = new ArrayList<String>();
		list.add("MOV R32 1");
		Machine m = new Machine();
		// the assertTrue method is used to check whether something holds.
		m.execute(list);
	}
	
	@Test(expected = InvalidInstructionException.class)
	public void test11() {
		List<String> list = new ArrayList<String>();
		list.add("MOV R0 65536");
		Machine m = new Machine();
		// the assertTrue method is used to check whether something holds.
		m.execute(list);
	}
	
	@Test(expected = InvalidInstructionException.class)
	public void test12() {
		List<String> list = new ArrayList<String>();
		list.add("MOV R0 -65536");
		Machine m = new Machine();
		// the assertTrue method is used to check whether something holds.
		m.execute(list);
	}
	
	@Test
	public void test13() {
		/* mutation: regs[src1] +1 */
		List<String> list = new ArrayList<String>();
		list.add("MOV R0 1");
		list.add("MOV R1 10");
		list.add("DIV R1 R0 R2");
		list.add("RET R1");
		Machine m = new Machine();
		// the assertTrue method is used to check whether something holds.
		assertEquals(10, m.execute(list));
	}
	
	@Test
	public void test14() {
		/* mutation: regs[src1] +1 */
		List<String> list = new ArrayList<String>();
		list.add("MOV R0 0");
		list.add("JZ R0 2");
		list.add("MOV R0 2");
		list.add("RET R0");
		Machine m = new Machine();
		// the assertTrue method is used to check whether something holds.
		assertEquals(0, m.execute(list));
	}
	
	@Test
	public void test15() {
		/* mutation: regs[src1] +1 */
		List<String> list = new ArrayList<String>();
		list.add("MOV R0 0");
		list.add("MOV R1 -2");
		list.add("LDR R1 R0 -65535");
		list.add("RET R1");
		Machine m = new Machine();
		// the assertTrue method is used to check whether something holds.
		assertEquals(-2, m.execute(list));
	}
}
